package net.khhs.SIP;

import net.khhs.SIP.InviterObjects.InviteMessage;
import net.khhs.SIP.InviterObjects.InviteOkMessage;
import net.khhs.SIP.InviterObjects.InviteRequestResponseMessage;
import net.khhs.SIP.MediaStreams.AudioStream;
import net.khhs.SIP.RegisterObjects.RegisterMessage;
import net.khhs.SIP.RegisterObjects.RegisterRequestMessage;
import net.khhs.SIP.RegisterObjects.RegisterRequestResponseMessage;

import javax.swing.*;
import java.io.IOException;
import java.net.*;
import java.util.Arrays;

public class SipPhone {
    private DatagramSocket ClientSock;
    private SocketAddress ServerAddress;
    private Contact SipContact;

    public SipPhone(String ipAddress){
        try {
            ClientSock = new DatagramSocket(0);
            ServerAddress = new InetSocketAddress(InetAddress.getByName(ipAddress)), 5060);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String register(Contact contact){
        try {
            contact.MyPort = ClientSock.getLocalPort();
            contact.MyIp = InetAddress.getLocalHost().getHostAddress();
            SipContact = contact;

            //REGISTER
            RegisterMessage RegMsg = new RegisterMessage(SipContact);
            SendUdpPacket(RegMsg.toByteArray());

            //Receive register details
            RegisterRequestMessage RegReqMsg = new RegisterRequestMessage(ReceiveUdpPacket());
            contact.setRemoteAddress(RegReqMsg.Via);
            RegisterRequestResponseMessage RegReqResMsg = new RegisterRequestResponseMessage(RegReqMsg, contact);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Send REGISTER details
            SendUdpPacket(RegReqResMsg.toByteArray());

            //Receive result message
            ReceiveUdpPacket();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Did not register";
        }
        return "ok";
    }

    public void call(Contact callingContact){
        InviteMessage InvMsg = new InviteMessage(SipContact, callingContact);
        SendUdpPacket(InvMsg.toByteArray());

        InviteRequestResponseMessage InvReqResMsg = new InviteRequestResponseMessage(InvMsg, ReceiveUdpPacket(), SipContact);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SendUdpPacket((InvReqResMsg.toByteArray()));





        System.out.println(new String(ReceiveUdpPacket()));
        while(true){
            String str = new String(ReceiveUdpPacket());
            if(str.contains("200 OK")){
                InviteOkMessage OkMsg = new InviteOkMessage(str);
                AudioStream strm = null;
                strm = new AudioStream(new InetSocketAddress(OkMsg.ServerIp, Integer.parseInt(OkMsg.ServerPort)),7078, 0);
                strm.startVoiceChat();
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void SendUdpPacket(byte[] udpData){
        try {
            DatagramPacket UdpPacket = new DatagramPacket(udpData, udpData.length, ServerAddress);
            ClientSock.send(UdpPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] ReceiveUdpPacket(){
        try {
            byte[] PacketBuffer = new byte[1400];
            DatagramPacket UdpPacket = new DatagramPacket(PacketBuffer, PacketBuffer.length);
            ClientSock.receive(UdpPacket);
            return Arrays.copyOfRange(PacketBuffer, 0, UdpPacket.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
