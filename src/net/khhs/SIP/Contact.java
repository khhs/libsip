package net.khhs.SIP;

public class Contact {
    public String Username;
    public String Password;
    public String Domain;
    public String CallId;
    public String MyIp;
    public int MyPort;
    public String RemoteIp;
    public String RemotePort;

    public Contact(){

    }

    public Contact(String username, String password, String domain){
        GenerateCallId();
        this.Username = username;
        this.Password = password;
        this.Domain = domain;
    }

    @Override
    public String toString(){
        return "sip:"+Username+"@"+Domain;
    }

    private void GenerateCallId(){
        CallId = (100000 + (int)(Math.random() * 999999))+"";
    }

    public void setRemoteAddress(String via){
        for(String str : via.trim().split(";")){
            if(str.startsWith("received")){
                RemoteIp = str.substring(str.indexOf("=")+1);
            }
            else if(str.startsWith("rport")){
                RemotePort = str.substring(str.indexOf("=")+1);
            }
        }
    }
}
