package net.khhs.SIP.MediaStreams;

import net.khhs.RTP.RtpPacket;
import net.khhs.RTP.RtpPacketHandler;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;

public class AudioStream {
    private DatagramSocket AudioSocket;
    private DatagramPacket AudioOutPacket;
    private RtpPacketHandler RtpHandler;
    private DatagramPacket AudioInPacket;

    private Thread AudioOutWorker;
    private Thread AudioInWorker;
    private SocketAddress ServerAddress;

    public AudioStream(SocketAddress sendIpAddress, int bindPort, int codec) {
        try {
            AudioSocket = new DatagramSocket(bindPort);
            AudioSocket.connect(sendIpAddress);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void startVoiceChat(){
        try {
            recordAudio();
            playAudio();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    private void recordAudio() throws LineUnavailableException {
        DataLine.Info AudioInInfo = new DataLine.Info(TargetDataLine.class, getFormat());
        final TargetDataLine AudioInLine = (TargetDataLine) AudioSystem.getLine(AudioInInfo);
        AudioInLine.open(getFormat());
        AudioInLine.start();

        AudioOutWorker = new Thread(() -> {
            byte[] BufferOut = new byte[((int) getFormat().getSampleRate() * getFormat().getFrameSize()) / getFormat().getSampleSizeInBits()];
            try {
                while (true) {
                    int count = AudioInLine.read(BufferOut, 0, BufferOut.length);
                    if (count > 0) {
                        sendAudio(BufferOut, count);
                    }
                }
            } catch (Exception e) {
                System.err.println("I/O problems: " + e);
            }
        });
        AudioOutWorker.start();
    }

    private void sendAudio(byte[] buffer, int count){
        new Thread(() ->{
            try {
                RtpPacket PacketOut = RtpHandler.createRtpPacket(buffer, count);
                byte[] UdpBuffer = PacketOut.toByteArray();
                AudioOutPacket = new DatagramPacket(UdpBuffer, UdpBuffer.length);
                AudioSocket.send(AudioOutPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private byte[] retrieveAudio(){
        byte[] buffer;
        try {
            buffer = new byte[1400];
            AudioInPacket = new DatagramPacket(buffer, buffer.length);
            AudioSocket.receive(AudioInPacket);
            RtpPacket InPack = new RtpPacket(AudioInPacket.getData());
            return InPack.getPayload();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void playAudio() throws LineUnavailableException {
        DataLine.Info AudioOutInfo = new DataLine.Info( SourceDataLine.class, getFormat());
        final SourceDataLine AudioOutLine = (SourceDataLine) AudioSystem.getLine(AudioOutInfo);
        AudioOutLine.open(getFormat());
        AudioOutLine.start();

        AudioInWorker = new Thread(() -> {
            while(true) {
                byte[] buffer = retrieveAudio();
                if (buffer != null) {
                    AudioOutLine.write(buffer, 0, buffer.length);
                }
            }
        });
        AudioInWorker.start();
    }

    private AudioFormat getFormat() {
        float sampleRate = 8000;
        int sampleSizeInBits = 8;
        int channels = 1;
        boolean signed = false;
        boolean bigEndian = false;
        return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }
}
