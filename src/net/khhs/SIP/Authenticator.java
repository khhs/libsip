package net.khhs.SIP;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Authenticator {

    private static String NonceCounter = "00000001";

    public static String GeneratePasswordString(Contact contact, String digestRealm, String nonce, String qop, String method, String authType){
        try {
            MessageDigest MD5 = MessageDigest.getInstance("MD5");
            int CNonce = (10000 + (int)(Math.random() * 99999));

            byte[] TmpHA1 = MD5.digest((contact.Username + ":" + digestRealm + ":" + contact.Password).getBytes());
            String HA1 = new BigInteger(1, TmpHA1).toString(16);
            byte[] TmpHA2 = MD5.digest((method+":sip:"+contact.Domain).getBytes());
            String HA2 = new BigInteger(1, TmpHA2).toString(16);
            byte[] TmpResponse = MD5.digest((HA1+":"+nonce+":"+NonceCounter+":"+CNonce+":"+qop+":"+HA2).getBytes());
            String Response = new BigInteger(1, TmpResponse).toString(16);

            String TmpReturn = "Digest username=\""+contact.Username+"\", realm=\""+digestRealm+"\", nonce=\""+nonce+"\", " +
                    "uri=\"sip:"+contact.Domain+"\", response=\""+Response+"\", cnonce=\""+CNonce+"\", nc="+NonceCounter+", qop="+qop+"\n";
            if(authType.equals(""))
                return "Authorization: "+TmpReturn;
            else{
                return "Proxy-Authorization: "+TmpReturn;
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }


        return null;
    }

}
