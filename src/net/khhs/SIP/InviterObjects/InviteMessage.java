package net.khhs.SIP.InviterObjects;

import net.khhs.SIP.Contact;

public class InviteMessage {
    String RequestLine;
    String Via;
    String To;
    String From;
    String MaxForwards = "Max-Forwards: 70\n";
    String Allow = "Allow: INVITE, CANCEL, BYE, MESSAGE, UPDATE\n";
    String CallId;
    int CSeq = 22;
    String Contact;
    String ContentType = "Content-Type: application/sdp\n";
    String ContentLength;
    String UserAgent = "User-Agent: Zoip\n";
    String SDP;


    public InviteMessage(Contact contact, Contact callingContact){
        RequestLine = "INVITE " + callingContact.toString() + " SIP/2.0\n";
        Via = "Via: SIP/2.0/UDP " + contact.MyIp+":"+contact.MyPort+";rport\n";
        From = "From: <"+contact.toString()+">\n";
        To = "To: "+callingContact.toString()+"\n";
        CallId = "Call-ID: "+contact.CallId+"\n";
        Contact = "Contact: <sip:"+contact.Username+"@"+contact.RemoteIp+":"+contact.RemotePort+";transport=udp>\n";
        SDP = generateSDP(contact);
        ContentLength = "Content-Length: " + SDP.length()+"\n";
    }

    private String generateSDP(Contact contact) {
        String TmpSDP =
        "v=0\n"+
        "o="+contact.Username+" 1848 1206 IN IP4 "+contact.MyIp+"\n"+
        "s=Talk\n"+
        "c=IN IP4 "+contact.MyIp+"\n"+
        "t=0 0\n"+
        "m=audio 7078 RTP/AVP 0 96\n"+
        "a=rtpmap:0 PCMU/8000\n"+
        "a=rtpmap:96 opus/48000/2\n"+
        "a=fmtp:96 useinbandfec=1\n";
        return TmpSDP;
    }

    private String getCSeqString(){
        return "CSeq: " + CSeq + " INVITE\n";
    }

    public String toStringWithoutSdp(){
        return RequestLine+Via+From+To+getCSeqString()+CallId+MaxForwards+Allow+ContentType+ContentLength+Contact+UserAgent;
    }

    public String getSdpString(){
        return SDP;
    }

    public byte[] toByteArray(){
        return (RequestLine+Via+From+To+MaxForwards+Allow+CallId+getCSeqString()+Contact+ContentType+ContentLength+UserAgent+"\n"+SDP).getBytes();
    }
}
