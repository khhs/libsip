package net.khhs.SIP.InviterObjects;

import net.khhs.SIP.Authenticator;
import net.khhs.SIP.Contact;
import net.khhs.SIP.RegisterObjects.RegisterMessage;

public class InviteRequestResponseMessage extends InviteRequestMessage{
    public InviteMessage InvMsg;
    private String DigestRealm;
    private String Nonce;
    private String Qop;
    private String Authorization;

    public InviteRequestResponseMessage(InviteMessage inviteMessage, byte[] invRequestMsg, Contact contact){
        super(invRequestMsg);

        this.InvMsg = inviteMessage;
        InvMsg.CSeq++;
        setRequiredAuthVariablesFromString(this.WwwAuthenticate);

        Authorization = Authenticator.GeneratePasswordString(contact, DigestRealm, Nonce, Qop,"INVITE", "proxy");
    }

    private void setRequiredAuthVariablesFromString(String authString){
        String tmpString = authString.toLowerCase();
        //Digest realm
        int tmp1 = tmpString.indexOf("\"", tmpString.indexOf("digest realm=\""))+1;
        int tmp2 = tmpString.indexOf('"', tmp1);
        DigestRealm = tmpString.substring(tmp1, tmp2);
        //nonce
        tmp1 = tmpString.indexOf("\"", tmpString.indexOf("nonce"))+1;
        tmp2 = tmpString.indexOf("\"", tmp1+1);
        Nonce = tmpString.substring(tmp1, tmp2);
        //Qop
        tmp1 = tmpString.indexOf("\"", tmpString.indexOf("qop"))+1;
        tmp2 = tmpString.indexOf('"', tmp1+1);
        Qop = tmpString.substring(tmp1, tmp2);
    }

    public byte[] toByteArray(){
        return (InvMsg.toStringWithoutSdp()+Authorization+"\n"+InvMsg.getSdpString()).getBytes();
    }
}
