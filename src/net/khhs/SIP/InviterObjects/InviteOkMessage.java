package net.khhs.SIP.InviterObjects;

import java.util.Arrays;

public class InviteOkMessage {

    String RequestLine;
    String Via;
    String To;
    String From;
    String Allow;
    String CallId;
    String CSeq;
    String Contact;
    String ContentType;
    String ContentLength;
    String UserAgent;
    String SDP;
    public String ServerIp;
    public String ServerPort;

    public InviteOkMessage(String udpData) {
        udpData = udpData.replace("\r","");
        for (String str : udpData.toLowerCase().split("\n")) {
            if (!str.equals("")) {
                if (str.startsWith("SIP/2.0")) {
                    RequestLine = str;
                } else if (str.startsWith("via:")) {
                    Via = str;
                } else if (str.startsWith("from:")) {
                    From = str;
                } else if (str.startsWith("to")) {
                    To = str;
                } else if (str.startsWith("cseq")) {
                    CSeq = str;
                } else if (str.startsWith("user-agent") || str.startsWith("useragent") || str.startsWith("server")) {
                    UserAgent = str;
                } else if (str.startsWith("allow")){
                    Allow = str;
                } else if (str.startsWith("call-id")){
                    CallId = str;
                } else if (str.startsWith("contact")){
                    Contact = str;
                } else if (str.startsWith("content-type")){
                    ContentType = str;
                } else if (str.startsWith("content-length")){
                    ContentLength = str;
                }
            }
        }
        SDP = new String(Arrays.copyOfRange(udpData.getBytes(), udpData.length()-Integer.parseInt(ContentLength.substring(ContentLength.indexOf(" ")+1)), udpData.length()));
        String[] SdpArr = SDP.split("\n");
        for(String str : SdpArr){
            if(str.startsWith("c=")){
                ServerIp = str.substring(str.indexOf("IP4 ")+4);
            }
            else if(str.startsWith("m=")){
                String tmp1 = str.substring(str.indexOf("audio ")+6);
                ServerPort = tmp1.substring(0, tmp1.indexOf(" "));
            }
        }
        int i = 0;
    }
}
