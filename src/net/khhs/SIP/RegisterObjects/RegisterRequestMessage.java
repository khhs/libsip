package net.khhs.SIP.RegisterObjects;

public class RegisterRequestMessage {
    public String RequestLine;
    public String Via;
    public String From;
    public String To;
    public String CSeq;
    public String UserAgent;
    public String WwwAuthenticate;

    public RegisterRequestMessage(byte[] udpData){
        for(String str : new String(udpData).toLowerCase().split("\n")){
            if(!str.equals("")){
                if(str.startsWith("SIP/2.0")){
                    RequestLine = str;
                }else if(str.startsWith("via:")){
                    Via = str;
                }else if(str.startsWith("from:")){
                    From = str;
                }else if(str.startsWith("to")){
                    To = str;
                }else if(str.startsWith("cseq")){
                    CSeq = str;
                }else if(str.startsWith("www-authenticate")){
                    WwwAuthenticate = str;
                }else if(str.startsWith("user-agent") || str.startsWith("useragent") || str.startsWith("server")){
                    UserAgent = str;
                }
            }
        }
    }

    public byte[] toByteArray(){
        return (RequestLine+Via+From+To+CSeq+UserAgent+WwwAuthenticate).getBytes();
    }
}
