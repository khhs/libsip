package net.khhs.SIP.RegisterObjects;

import net.khhs.SIP.Authenticator;
import net.khhs.SIP.Contact;

public class RegisterRequestResponseMessage {
    public RegisterMessage RegMessage;
    private String DigestRealm;
    private String Nonce;
    private String Qop;
    private String Authorization;

    public RegisterRequestResponseMessage(RegisterRequestMessage requestMessage, Contact contact){
        this.RegMessage = new RegisterMessage(contact);
        RegMessage.CSeq++;
        setRequiredAuthVariablesFromString(requestMessage.WwwAuthenticate);

        Authorization = Authenticator.GeneratePasswordString(contact, DigestRealm, Nonce, Qop,"REGISTER", "");
    }

    private void setRequiredAuthVariablesFromString(String authString){
        String tmpString = authString.toLowerCase();
        //Digest realm
        int tmp1 = tmpString.indexOf("\"", tmpString.indexOf("digest realm=\""))+1;
        int tmp2 = tmpString.indexOf('"', tmp1);
        DigestRealm = tmpString.substring(tmp1, tmp2);
        //nonce
        tmp1 = tmpString.indexOf("\"", tmpString.indexOf("nonce"))+1;
        tmp2 = tmpString.indexOf("\"", tmp1+1);
        Nonce = tmpString.substring(tmp1, tmp2);
        //Qop
        tmp1 = tmpString.indexOf("\"", tmpString.indexOf("qop"))+1;
        tmp2 = tmpString.indexOf('"', tmp1+1);
        Qop = tmpString.substring(tmp1, tmp2);
    }

    public byte[] toByteArray(){
        return (RegMessage.toString()+Authorization+"\n").getBytes();
    }
}
