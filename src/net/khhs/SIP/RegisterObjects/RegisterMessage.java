package net.khhs.SIP.RegisterObjects;

import net.khhs.SIP.Contact;

public class RegisterMessage {

    public String RequestLine;
    public String To;
    public String From;
    public String Via;
    public String MaxForwards;
    public String CallId;
    public int CSeq;
    public String Expires;
    public String Contact;
    public String ContentLength;
    public String UserAgent;


    public RegisterMessage(Contact contact) {
        CSeq = 1;
        RequestLine = "REGISTER sip:" + contact.Domain + " SIP/2.0\n";
        To = "To: " + contact.toString() + "\n";
        From = "From: <" + contact.toString() + ">\n";
        Via = "Via: SIP/2.0/UDP " + contact.MyIp + ":"+contact.MyPort+";rport\n";
        MaxForwards = "Max-Forwards: 70\n";
        CallId = "Call-ID: " + contact.CallId + "\n";
        Expires = "Expires: 3600\n";
        if(contact.RemoteIp == null)
            Contact = "Contact: <sip:"+contact.Username+"@"+contact.MyIp+";transport=udp>\n";
        else
            Contact = "Contact: <sip:"+contact.Username+"@"+contact.RemoteIp+":"+contact.RemotePort+";transport=udp>\n";
        ContentLength = "Content-Length: 0\n";
        UserAgent = "User-Agent: Zoip\n";
    }

    private String getCSeqString(){
        return "CSeq: " + CSeq + " REGISTER\n";
    }

    public byte[] toByteArray(){
        return (RequestLine + To + From + Via + MaxForwards + CallId + getCSeqString() + Expires + Contact + ContentLength + UserAgent+"\n").getBytes();
    }

    @Override
    public String toString(){
        return RequestLine + To + From + Via + MaxForwards + CallId + getCSeqString() + Expires + Contact + ContentLength + UserAgent;
    }
}
